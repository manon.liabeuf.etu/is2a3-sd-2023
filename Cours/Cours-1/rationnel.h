/* rationnel.h */

struct rationnel {
    int numer;
    int denom;
};

/* 
   Spécifications du type
   Le type struct rationnel implante des nombres rationnels
   Le champ numer contient le numérateur
   Le champ denom contient le dénominateur.
   Le dénominateur est strictement positif.
   La fraction est réduite (le pgcd des deux champs vaut 1)
 */

// Constructeur. Affecte à R le rationnel a/b
extern void init_rationnel (struct rationnel* R, int a, int b);

// Destructeur
extern void clear_rationnel (struct rationnel*);

// Affecte A + B à S (rq S en mode R. A et B en mode D).
extern void add_rationnel (struct rationnel* S, 
                           struct rationnel* A, struct rationnel* B);

// Affichage
extern void print_rationnel (struct rationnel);

