/* rationnel. c*/

#include <stdio.h>
#include <assert.h>
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

// Constructeur. Affecte à R le rationnel a/b
void init_rationnel (struct rationnel* R, int a, int b)
{   int g;

    assert (b != 0);    // J'affirme que b != 0

    g = Euclide (a, b);
    if (b > 0)
    {   
        R->numer = a / g;
        R->denom = b / g;
    } else
    {   
        R->numer = - a / g;
        R->denom = - b / g;
    }
}

// Destructeur
void clear_rationnel (struct rationnel* R)
{
}

// Affecte A + B à S (rq S en mode R. A et B en mode D).
void add_rationnel (struct rationnel* S,
                           struct rationnel* A, struct rationnel* B)
{
// Attention : bug !
    int p, q;
    p = A->numer * B->denom + A->denom * B->numer;
    q = A->denom * B->denom;
    init_rationnel (S, p, q);
}

// Affichage
void print_rationnel (struct rationnel R)
{
    printf ("%d/%d\n", R.numer, R.denom);
}


