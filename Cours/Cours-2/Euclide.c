/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

