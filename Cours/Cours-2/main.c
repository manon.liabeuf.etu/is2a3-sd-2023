/* main.c */

#include "rationnel.h"

int main ()
{   struct rationnel A, B;

    init_rationnel (&A, 6, 8);
    init_rationnel (&B, -5, -2);
    add_rationnel (&A, &A, &B);     /* A = A + B */
    print_rationnel (A);
    clear_rationnel (&A);
    clear_rationnel (&B);
    return 0;
}

