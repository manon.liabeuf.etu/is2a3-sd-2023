// Bon programme

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
 * Ce programme marche.
 * 
 * La fonction malloc retourne l'adresse d'une zone mémoire située
 * dans le tas et donc à l'extérieur de la pile d'exécution du processus.
 */

int main ()
{
  char *p;

  p = (char *) malloc (50 * sizeof (char));
  strcpy (p, "un elephant dans un magasin de porcelaine");
  printf ("%s\n", p);
  free (p);
  return 0;
}
