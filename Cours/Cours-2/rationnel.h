/* rationnel.h */

struct rationnel {
    int* data;
};

/* 
   Spécifications du type
   Le type struct rationnel implante des nombres rationnels
   La champ data pointe vers un tableau de 2 int alloués dynamiquement
   Le champ data[0] contient le numérateur
   Le champ data[1] contient le dénominateur.
   Le dénominateur est strictement positif.
   La fraction est réduite (le pgcd des deux champs vaut 1)
 */

// Constructeur. Affecte à R le rationnel a/b
extern void init_rationnel (struct rationnel* R, int a, int b);

// Destructeur
extern void clear_rationnel (struct rationnel*);

// Affecte A + B à S (rq S en mode R. A et B en mode D).
extern void add_rationnel (struct rationnel* S, 
                           struct rationnel* A, struct rationnel* B);

// Affichage
extern void print_rationnel (struct rationnel);

