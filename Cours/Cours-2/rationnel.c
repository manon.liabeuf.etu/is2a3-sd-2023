/* rationnel. c*/

#include <stdlib.h> // pour malloc
#include <stdio.h>
#include <assert.h>
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

// Affecte à R le rationnel a/b
static void set_rationnel (struct rationnel* R, int a, int b)
{   int g;

// Pas un constructeur => pas de malloc !

    g = Euclide (a, b);
    if (b > 0)
    {   
        R->data[0] = a / g;
        R->data[1] = b / g;
    } else
    {   
        R->data[0] = - a / g;
        R->data[1] = - b / g;
    }
}

// Constructeur. Affecte à R le rationnel a/b
void init_rationnel (struct rationnel* R, int a, int b)
{ 

    assert (b != 0);    // J'affirme que b != 0

    R->data = (int*) malloc (2 * sizeof(int));
    set_rationnel (R, a, b);
}

// Destructeur
void clear_rationnel (struct rationnel* R)
{
    free (R->data);     // free répond au malloc
}

// Affecte A + B à S (rq S en mode R. A et B en mode D).
void add_rationnel (struct rationnel* S,
                           struct rationnel* A, struct rationnel* B)
{
// Attention : bug !
    int p, q;
    p = A->data[0] * B->data[1] + A->data[1] * B->data[0];
    q = A->data[1] * B->data[1];
//    init_rationnel (S, p, q);
    set_rationnel (S, p, q);
}

// Affichage
void print_rationnel (struct rationnel R)
{
    printf ("%d/%d\n", R.data[0], R.data[1]);
}


