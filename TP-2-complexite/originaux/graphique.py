# Suppose que les fichiers 'Karatsuba.stats' et 'Elementaire.stats'
# sont disponibles

# Commande : python3 graphique.py

import numpy as np
import matplotlib.pyplot as plt
K = np.loadtxt ('Karatsuba.stats')
E = np.loadtxt ('Elementaire.stats')
plt.plot (K[:,0], K[:,1], label='Karatsuba')
plt.plot (E[:,0], E[:,1], label='Élémentaire')
plt.legend ()
plt.show ()
